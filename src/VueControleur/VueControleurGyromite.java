package VueControleur;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;

import modele.deplacements.Controle4Directions;
import modele.deplacements.ControleColonneDeplacement;
import modele.deplacements.Direction;
import modele.plateau.*;


/** Cette classe a deux fonctions :
 *  (1) Vue : proposer une représentation graphique de l'application (cases graphiques, etc.)
 *  (2) Controleur : écouter les évènements clavier et déclencher le traitement adapté sur le modèle (flèches direction Pacman, etc.))
 *
 */
public class VueControleurGyromite implements Observer {
    private Jeu jeu; // référence sur une classe de modèle : permet d'accéder aux données du modèle pour le rafraichissement, permet de communiquer les actions clavier (ou souris)

    private int sizeX; // taille de la grille affichée
    private int sizeY;

    // icones affichées dans la grille
    private ImageIcon icoHero;
    private ImageIcon icoVide;
    private ImageIcon icoMur;
    private ImageIcon icoEchelle;
    private ImageIcon icoColonneBleuVertical;
    private ImageIcon icoColonneRougeVertical;
    private ImageIcon icoColonneBleuHorizontal;
    private ImageIcon icoColonneRougeHorizontal;
    private ImageIcon icoBot;
    private ImageIcon icoBombe;
    private ImageIcon icoFinWin;
    private ImageIcon icoFinLoose;

    private JLabel[][] tabJLabel; // cases graphique (au moment du rafraichissement, chaque case va être associée à une icône, suivant ce qui est présent dans le modèle)
    private JPanel panelInformation;
    private JFrame Menu,Jeu,FinLoose,FinWin;

    public VueControleurGyromite(Jeu _jeu) {
        sizeX = jeu.SIZE_X;
        sizeY = _jeu.SIZE_Y;
        jeu = _jeu;

        chargerLesIcones();
        placerLesComposantsGraphiques();
        ajouterEcouteurClavier();
    }

    private void ajouterEcouteurClavier() {
        Jeu.addKeyListener(new KeyAdapter() { // new KeyAdapter() { ... } est une instance de classe anonyme, il s'agit d'un objet qui correspond au controleur dans MVC
            @Override
            public void keyPressed(KeyEvent e) {
                switch(e.getKeyCode()) {  // on regarde quelle touche a été pressée
                    case KeyEvent.VK_LEFT : Controle4Directions.getInstance().setDirectionCourante(Direction.gauche); break;
                    case KeyEvent.VK_RIGHT : Controle4Directions.getInstance().setDirectionCourante(Direction.droite); break;
                    case KeyEvent.VK_DOWN : Controle4Directions.getInstance().setDirectionCourante(Direction.bas); break;
                    case KeyEvent.VK_UP : Controle4Directions.getInstance().setDirectionCourante(Direction.haut); break;
                    case KeyEvent.VK_M : ControleColonneDeplacement.getInstance().setType(0); 
                                         ControleColonneDeplacement.getInstance().changeDirectionCourante();break;

                    case KeyEvent.VK_P : ControleColonneDeplacement.getInstance().setType(1); 
                                         ControleColonneDeplacement.getInstance().changeDirectionCourante();break;

                    
                                         
                }
            }
        });
    }


    private void chargerLesIcones() {
        icoHero = chargerIcone("Images/Hector.png");
        icoVide = chargerIcone("Images/Vide.png");
        icoColonneBleuVertical = chargerIcone("Images/Colonne_bleuV.png");
        icoColonneRougeVertical = chargerIcone("Images/Colonne_rougeV.png");
        icoColonneBleuHorizontal= chargerIcone("Images/Colonne_bleuH.png");
        icoColonneRougeHorizontal = chargerIcone("Images/Colonne_rougeH.png");
        icoMur = chargerIcone("Images/Mur.png");
        icoEchelle = chargerIcone("Images/Echelle.png");
        icoBot = chargerIcone("Images/Bot.png");
        icoBombe = chargerIcone("Images/Bombe.png");
        icoFinWin = chargerIcone("Images/FinWin.png");
        icoFinLoose = chargerIcone("Images/FinLoose.png");
    }

    private ImageIcon chargerIcone(String urlIcone) {
        BufferedImage image = null;

        try {
            image = ImageIO.read(new File(urlIcone));
        } catch (IOException ex) {
            Logger.getLogger(VueControleurGyromite.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

        return new ImageIcon(image);
    }

    private void placerLesComposantsGraphiques() {
        Menu = new JFrame();
        Menu.setTitle("Menu");
        Menu.setSize(400, 250);
        Menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        JButton panelMenu = new JButton("Start");
        panelMenu.setFont(panelMenu.getFont().deriveFont(60f));
        panelMenu.setLayout(new BorderLayout(0,0));
        panelMenu.setSize(400,15);
        panelMenu.setVisible(true); 
        panelMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Menu.setVisible(false);
                Jeu.setVisible(true);
            }
        });
        Menu.add(panelMenu,BorderLayout.CENTER);
         
        Jeu = new JFrame();
        Jeu.setTitle("Gyromite");
        Jeu.setSize(400, 250);
        Jeu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // permet de terminer l'application à la fermeture de la fenêtre

        //Champs information
        panelInformation = new JPanel();
        panelInformation.setLayout(new BorderLayout(0,0));
        panelInformation.setSize(400,15);
        panelInformation.setVisible(true);
        
        //Score
        JLabel score = new JLabel("");
        score.setFont(score.getFont().deriveFont(15f));

        //Vies
        JLabel vies = new JLabel("");
        vies.setFont(score.getFont());

        panelInformation.add(vies,BorderLayout.WEST);
        panelInformation.add(score,BorderLayout.EAST);
        Jeu.add(panelInformation,BorderLayout.NORTH);
        
        JComponent grilleJLabels = new JPanel(new GridLayout(sizeY, sizeX)); // grilleJLabels va contenir les cases graphiques et les positionner sous la forme d'une grille
        tabJLabel = new JLabel[sizeX][sizeY];
        
        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                JLabel jlab = new JLabel();
                tabJLabel[x][y] = jlab; // on conserve les cases graphiques dans tabJLabel pour avoir un accès pratique à celles-ci (voir mettreAJourAffichage() )
                grilleJLabels.add(jlab);
            }
        }
        Jeu.add(grilleJLabels);
        
        FinLoose = new JFrame();
        FinLoose.setTitle("Gyromite");
        FinLoose.setSize(400, 250);
        FinLoose.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        JLabel LabelFinLoose = new JLabel("");
        LabelFinLoose.setLayout(new BorderLayout(0,0));
        LabelFinLoose.setFont(LabelFinLoose.getFont().deriveFont(15f));
        LabelFinLoose.setIcon(icoFinLoose);
        FinLoose.add(LabelFinLoose,BorderLayout.CENTER);
        
        FinWin = new JFrame();
        FinWin.setTitle("Gyromite");
        FinWin.setSize(400, 250);
        FinWin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        JLabel LabelFinWin = new JLabel("");
        LabelFinWin.setLayout(new BorderLayout(0,0));
        LabelFinWin.setFont(LabelFinWin.getFont().deriveFont(15f));
        LabelFinWin.setIcon(icoFinWin);
        FinWin.add(LabelFinWin,BorderLayout.CENTER);
    
        
    }

    
    /**
     * Il y a une grille du côté du modèle ( jeu.getGrille() ) et une grille du côté de la vue (tabJLabel)
     */
    private void mettreAJourAffichage() {
        ((JLabel)panelInformation.getComponent(0)).setText("Vies restantes: " + Integer.toString(jeu.getVies()));
        ((JLabel)panelInformation.getComponent(1)).setText("Score:" + Integer.toString(jeu.getScore()));
        ArrayList<Entite> entitesXY;
        int entiteAAfficher;
        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                entitesXY = jeu.getGrille()[x][y];
                if (entitesXY.size() == 1) {
                    entiteAAfficher = 0;
                }
                else{
                    entiteAAfficher = 1;
                }
                
                if(!entitesXY.isEmpty()){
                        if (entitesXY.get(entiteAAfficher) instanceof Heros) { // si la grille du modèle contient un Pacman, on associe l'icône Pacman du côté de la vue
                        // System.out.println("Héros !");
                        tabJLabel[x][y].setIcon(icoHero);
                    } else if (entitesXY.get(entiteAAfficher) instanceof Mur) {
                        tabJLabel[x][y].setIcon(icoMur);
                    } else if (entitesXY.get(entiteAAfficher) instanceof Colonne) {
                        Colonne c = (Colonne) entitesXY.get(entiteAAfficher);
                        if(c.getType() == 0){
                            if(c.getDirection() == 0){
                                 tabJLabel[x][y].setIcon(icoColonneBleuVertical);
                            }
                            else{
                                 tabJLabel[x][y].setIcon(icoColonneBleuHorizontal);
                            }
                           
                        } else{
                            if(c.getDirection() == 0){
                                tabJLabel[x][y].setIcon(icoColonneRougeVertical);
                            }
                            else{
                                tabJLabel[x][y].setIcon(icoColonneRougeHorizontal);
                            }
                        }
                    } else if (entitesXY.get(entiteAAfficher) instanceof Bot) {
                        tabJLabel[x][y].setIcon(icoBot);
                    } else if (entitesXY.get(entiteAAfficher) instanceof Echelle) {
                        tabJLabel[x][y].setIcon(icoEchelle);
                    } else if(entitesXY.get(entiteAAfficher) instanceof Bombe){
                        tabJLabel[x][y].setIcon(icoBombe);
                    }
                } 
                else {
                        tabJLabel[x][y].setIcon(icoVide);
                }
                
                
            }

        }
    }

    @Override
    public void update(Observable o, Object arg) {
        mettreAJourAffichage();
        if(jeu.finLoose()){
            Jeu.setVisible(false);
            Jeu.dispose();
            FinLoose.setVisible(true);
        }
        if(jeu.finWin()){
            Jeu.setVisible(false);
            Jeu.dispose();
            FinWin.setVisible(true);
        }
        /*
        SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        mettreAJourAffichage();
                    }
                }); 
        */

    }
    
    public void Afficher(){
        Menu.setVisible(true);
        
    }
}
