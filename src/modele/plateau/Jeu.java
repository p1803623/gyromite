/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.plateau;

import modele.deplacements.Controle4Directions;
import modele.deplacements.Direction;
import modele.deplacements.Gravite;
import modele.deplacements.Ordonnanceur;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import modele.deplacements.ControleColonneDeplacement;
import modele.deplacements.IA;

/** Actuellement, cette classe gère les postions
 * (ajouter conditions de victoire, chargement du plateau, etc.)
 */
public class Jeu {

    public static final int SIZE_X = 20;
    public static final int SIZE_Y = 10;
    public static final int NIVEAU_MAX = 8;
    
    private static int vies = 5;
    

    // compteur de déplacements horizontal et vertical (1 max par défaut, à chaque pas de temps)
    private HashMap<Entite, Integer> cmptDeplH = new HashMap<Entite, Integer>();
    private HashMap<Entite, Integer> cmptDeplV = new HashMap<Entite, Integer>();

    private Heros hector;
    public String status = "EnCours";
    private int nbBombe = 0;
    private int niveauActuel = 1;
    private int score = 0;

    private HashMap<Entite, Point> map = new  HashMap<Entite, Point>(); // permet de récupérer la position d'une entité à partir de sa référence
    private ArrayList<Entite>[][] grilleEntites = new ArrayList[SIZE_X][SIZE_Y]; // permet de récupérer une entité à partir de ses coordonnées
    
    private Ordonnanceur ordonnanceur = new Ordonnanceur(this);
    
    

    public Jeu() throws IOException {
        // Initialisation des arraylist dans grille entites.
        for(int i = 0; i<SIZE_X;i++)
        {
            for(int j = 0; j<SIZE_Y; j++)
            {
                grilleEntites[i][j] = new ArrayList<Entite>();
                
            }
        }
        
        
        chargerMap("map1.txt");
        ordonnanceur.add(Gravite.getInstance());
        ordonnanceur.add(Controle4Directions.getInstance());
        ordonnanceur.add(IA.getInstance());
        ordonnanceur.add(ControleColonneDeplacement.getInstance()); 
    }

    public void resetCmptDepl() {
        cmptDeplH.clear();
        cmptDeplV.clear();
    }

    public void start(long _pause) {
        ordonnanceur.start(_pause);
    }
    
    private void clear(){
        ControleColonneDeplacement.getInstance().clear();
        ControleColonneDeplacement.getInstance().reset();

        Controle4Directions.getInstance().clear();
        IA.getInstance().clear();
        Gravite.getInstance().clear();
        map.clear();
        nbBombe = 0; 
        
        for(int i = 0; i<SIZE_X;i++)
        {
            for(int j = 0; j<SIZE_Y; j++)
            {
                grilleEntites[i][j].clear();
                
            }
        }
    }
    
    public void restart() throws IOException{
       clear();
       String mapACharger = "map" + niveauActuel + ".txt";
       chargerMap(mapACharger);
       status = "EnCours";
    }
    
    public ArrayList<Entite>[][] getGrille() {
        return grilleEntites;
    }
    
    public Heros getHector() {
        return hector;
    }
    
    public int getVies(){
        return this.vies;
    }
    
    public int getScore(){
        return this.score;
    }
    
    private void chargerMap(String nomDuFichier) throws IOException {
        Colonne colonneBleu, colonneRouge;
        Bot smick; 
          
        hector = new Heros(this);
        Controle4Directions.getInstance().addEntiteDynamique(hector);
        Gravite.getInstance().addEntiteDynamique(hector);
        
       try {
            // le fichier d'entree
            File fichier = new File("./Map/" + nomDuFichier);
            // l'objet file reader
            FileReader read = new FileReader(fichier);
            // Creation de l'objet BufferedReader
            BufferedReader bf = new BufferedReader(read);
            String ligne;
            char charEntite;
            for (int j = 0; j < SIZE_Y; j++) {
                ligne = bf.readLine();
                
                // affichage map dans la console 
                System.out.println(ligne);
                
                for (int i = 0; i <SIZE_X; i++) {
                    charEntite = ligne.charAt(i); 
                    switch(charEntite){
                        case 'M': addEntite(new Mur(this), i, j);
                        break;
                        case 'H': addEntite(hector, i, j);
                        break;
                        case 'E': addEntite(new Echelle(this), i, j);
                        break;
                        case 'B': colonneBleu = new Colonne(this, 0, 0);
                                  ControleColonneDeplacement.getInstance().addEntiteDynamique(colonneBleu);
                                  addEntite(colonneBleu,i,j);
                        break;
                        case 'R': colonneRouge = new Colonne(this, 1, 0);
                                  ControleColonneDeplacement.getInstance().addEntiteDynamique(colonneRouge);
                                  addEntite(colonneRouge,i,j);
                        break;
                        case 'b': colonneBleu = new Colonne(this, 0, 1);
                                  ControleColonneDeplacement.getInstance().addEntiteDynamique(colonneBleu);
                                  addEntite(colonneBleu,i,j);
                        break;
                        case 'r': colonneRouge = new Colonne(this, 1, 1);
                                  ControleColonneDeplacement.getInstance().addEntiteDynamique(colonneRouge);
                                  addEntite(colonneRouge,i,j);
                        break;
                        case 'S': smick = new Bot(this);
                                  Gravite.getInstance().addEntiteDynamique(smick);
                                  IA.getInstance().addEntiteDynamique(smick);
                                  addEntite(smick, i, j); 
                        break;
                        case 'D': addEntite(new Bombe(this), i, j);
                                  nbBombe++;
                        break;
                        
                    }
                } 
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        
  
    }

    private void addEntite(Entite e, int x, int y) {
        grilleEntites[x][y].add(e);
        map.put(e, new Point(x, y));
    }
    
    private void deleteEntite(Entite e, int x, int y){
        grilleEntites[x][y].remove(e);
        map.remove(e);
    }
    
    /** Permet par exemple a une entité  de percevoir sont environnement proche et de définir sa stratégie de déplacement
     *
     */
    public Entite regarderDansLaDirection(Entite e, Direction d) {
        Point positionEntite = map.get(e);
        return objetALaPosition(calculerPointCible(positionEntite, d));
    }
    
    
    private void perdreUneVie(){
        if(vies > 0){
            vies--;
            System.out.print("Il vous reste ");
            System.out.print(vies);
            System.out.println(" vies ");
            
            status = "restart";
          
            
        }else {
            System.out.println("Vous avez perdu !");
            status = "fin";
        }
    }
    
    private void passerNiveauSuivant (){
        niveauActuel++;
        if(niveauActuel > NIVEAU_MAX){
            status = "fin";          
        }
        else{
            status = "restart";
        }
       
    }
    
    private boolean gestionCollision(Entite entiteEnDeplacement, Entite entiteCible) {
        if(entiteCible == null || entiteCible.peutPermettreDeMonterDescendre()){
            return true;
        }
        
        if(entiteEnDeplacement instanceof Heros && entiteCible instanceof Bombe){
            nbBombe--;
            score += 100;
            Point pEntite = map.get(entiteCible);
            deleteEntite(entiteCible, pEntite.x, pEntite.y);
            
            if(nbBombe == 0){
                passerNiveauSuivant();
            }
        }
        
        if((entiteEnDeplacement instanceof Heros && entiteCible instanceof Bot) ||
            (entiteCible instanceof Heros && entiteEnDeplacement instanceof Bot)){
            perdreUneVie();
            return false;
        }
        
        if((entiteEnDeplacement instanceof Colonne && entiteCible.peutEtreEcrase())){
            if(entiteCible instanceof Heros){
                perdreUneVie();
            }
            else{
                Point pEntite = map.get(entiteCible);
                deleteEntite(entiteCible, pEntite.x, pEntite.y); 
                IA.getInstance().deleteEntiteDynamique((EntiteDynamique) entiteCible);
                Gravite.getInstance().deleteEntiteDynamique((EntiteDynamique) entiteCible);
                        
            }
            return false;
        }
        
        if(entiteCible.peutServirDeSupport()){
            return false;
        }

        return true;
    }
    

    
    /** Si le déplacement de l'entité est autorisé (pas de mur ou autre entité), il est réalisé
     * Sinon, rien n'est fait.
     */
    public boolean deplacerEntite(Entite e, Direction d){
        boolean retour = false;
        
        Point pCourant = map.get(e);
        
        Point pCible = calculerPointCible(pCourant, d);
        
        Entite entiteCible = objetALaPosition(pCible);
        boolean deplacement = gestionCollision(e, entiteCible);
        
        if (deplacement) {
            switch (d) {
                case bas:
                case haut:
                    if (cmptDeplV.get(e) == null) {
                        cmptDeplV.put(e, 1);
                        retour = true;
                    } 
                    break;
                case gauche:
                case droite:
                    if (cmptDeplH.get(e) == null) {
                        cmptDeplH.put(e, 1);
                        retour = true;
                    } 
                    break;
            }
        }
            
        
        if (retour) {
            deplacerEntite(pCourant, pCible, e);
        }
        return retour;
    }
    
    
    private Point calculerPointCible(Point pCourant, Direction d) {
        Point pCible = null;
        
        switch(d) {
            case haut: pCible = new Point(pCourant.x, pCourant.y - 1); break;
            case bas : pCible = new Point(pCourant.x, pCourant.y + 1); break;
            case gauche : pCible = new Point(pCourant.x - 1, pCourant.y); break;
            case droite : pCible = new Point(pCourant.x + 1, pCourant.y); break;     
            
        }
        
        return pCible;
    }
    
    private void deplacerEntite(Point pCourant, Point pCible, Entite e) {
        grilleEntites[pCourant.x][pCourant.y].remove(e);


        grilleEntites[pCible.x][pCible.y].add(e);
        map.put(e, pCible);
    }
    
    /** Indique si p est contenu dans la grille
     */
    private boolean contenuDansGrille(Point p) {
        return p.x >= 0 && p.x < SIZE_X && p.y >= 0 && p.y < SIZE_Y;
    }
    
    private Entite objetALaPosition(Point p) {
        Entite retour = null;
        
        if (contenuDansGrille(p) && !grilleEntites[p.x][p.y].isEmpty()) {
            if(grilleEntites[p.x][p.y].size() > 1){
                retour = grilleEntites[p.x][p.y].get(grilleEntites[p.x][p.y].size()-1);
            }
            else{
                retour = grilleEntites[p.x][p.y].get(0);
            }
            
        }
        
        return retour;
    }

    public Ordonnanceur getOrdonnanceur() {
        return ordonnanceur;
    }
   
    public boolean finLoose() {
        if (getVies() == 0)
                 {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean finWin(){
        if(nbBombe == 0 && niveauActuel > NIVEAU_MAX){
            return true;
        }
        else{
            return false;
        }
    }
}
