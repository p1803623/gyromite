package modele.plateau;

public class Colonne extends EntiteDynamique {
    private final int type; // 0 = bleu , 1= rouge
    private final int direction; // 0 = vertical, 1 = horizontal
    
    public Colonne(Jeu _jeu, int type, int direction) { 
        super(_jeu); 
        this.type = type; 
        this.direction = direction;
    }
    
    public int getType(){
        return type;
    }
    
    public int getDirection(){
        return direction;
    }

    public boolean peutEtreEcrase() { return false; }
    public boolean peutServirDeSupport() { return true; }
    public boolean peutPermettreDeMonterDescendre() { return false; };
}
