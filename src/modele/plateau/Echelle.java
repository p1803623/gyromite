
package modele.plateau;

/**
 *
 * @author gossfire
 */
public class Echelle extends EntiteStatique {
    public Echelle(Jeu _jeu) { super(_jeu); }
    @Override
    public boolean peutServirDeSupport() { return true; }
    @Override
    public boolean peutPermettreDeMonterDescendre() { return true; };
}
