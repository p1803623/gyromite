/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.plateau;

import modele.deplacements.Direction;

/**
 * Ennemis (Smicks)
 */
public class Bot extends EntiteDynamique {
    private Direction directionCourante = Direction.droite;
    private Direction ancienneDirection;

    public Direction getDirectionCourante() {
        return directionCourante;
    }

    public void setDirectionCourante(Direction directionCourante) {
        this.directionCourante = directionCourante;
    }

    public Direction getAncienneDirection() {
        return ancienneDirection;
    }

    public void setAncienneDirection(Direction ancienneDirection) {
        this.ancienneDirection = ancienneDirection;
    }

    public Bot(Jeu _jeu) {
        super(_jeu);
    }

    public boolean peutEtreEcrase() { return true; }
    public boolean peutServirDeSupport() { return true; }
    public boolean peutPermettreDeMonterDescendre() { return false; };
}
