
package modele.plateau;


public class Bombe extends EntiteStatique {
    public Bombe(Jeu _jeu) { super(_jeu); }
    
    @Override
    public boolean peutServirDeSupport() { return false; }
    
}
