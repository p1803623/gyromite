package modele.deplacements;

import java.io.IOException;
import modele.plateau.Jeu;

import java.util.ArrayList;
import java.util.Observable;

import static java.lang.Thread.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Ordonnanceur extends Observable implements Runnable {
    private Jeu jeu;
    private ArrayList<RealisateurDeDeplacement> lstDeplacements = new ArrayList<RealisateurDeDeplacement>();
    private long pause;
    public void add(RealisateurDeDeplacement deplacement) {
        lstDeplacements.add(deplacement);
    }

    public Ordonnanceur(Jeu _jeu) {
        jeu = _jeu;
    }

    public void start(long _pause) {
        pause = _pause;
        new Thread(this).start();
    }


    @Override
    public void run(){
        boolean update = false;

        while(true) {
            switch (jeu.status) {
                case "restart":
                    try {
                        jeu.restart();
                    } catch (IOException ex) {
                        Logger.getLogger(Ordonnanceur.class.getName()).log(Level.SEVERE, null, ex);
                    }
                break;

                case "fin":
                    System.err.println("FIN");             
                break;
                default:
                    jeu.resetCmptDepl();
                    for (RealisateurDeDeplacement d : lstDeplacements) {
                        if (d.realiserDeplacement()) {
                            update = true;
                        }
                    }

                    Controle4Directions.getInstance().resetDirection();

                    if (update) {
                        setChanged();
                        notifyObservers();
                    }

                    try {
                        sleep(pause);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                break;
            }
            
            
        }

    }
}
