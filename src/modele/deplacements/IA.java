package modele.deplacements;

import modele.plateau.Bot;
import modele.plateau.Entite;
import modele.plateau.EntiteDynamique;

public class IA extends RealisateurDeDeplacement {
    
    private static IA ia;

    public static IA getInstance() {
        if (ia == null) {
            ia = new IA();
        }
        return ia;
    }
    
    @Override
    protected boolean realiserDeplacement() {
        boolean ret = false;
        Direction directionCourante, ancienneDirection;
        for (EntiteDynamique e : lstEntitesDynamiques) {
            Bot smick = (Bot) e;
            directionCourante = smick.getDirectionCourante();
            ancienneDirection = smick.getAncienneDirection();
            Entite eDirection = e.regarderDansLaDirection(directionCourante);
            if (e.avancerDirectionChoisie(directionCourante)) {
                ret = true;
            } else {
                if (directionCourante == Direction.droite || directionCourante == Direction.gauche) {
                    directionCourante = (directionCourante == Direction.droite) ? Direction.gauche : Direction.droite;
                    smick.setDirectionCourante(directionCourante);
                } else {
                    smick.setDirectionCourante(ancienneDirection);
                }
            }

            if (eDirection != null && eDirection.peutPermettreDeMonterDescendre()) {
                if (directionCourante != Direction.haut) {
                    smick.setAncienneDirection(directionCourante);
                }

                smick.setDirectionCourante(Direction.haut);
            }
        }

        return ret;
    } 
}
