package modele.deplacements;

import modele.plateau.Colonne;
import modele.plateau.Entite;
import modele.plateau.EntiteDynamique;

/**
 * A la reception d'une commande, toutes les cases (EntitesDynamiques) des colonnes se déplacent dans la direction définie
 * (vérifier "collisions" avec le héros)
 */
public class ControleColonneDeplacement extends RealisateurDeDeplacement {
    private Direction directionCouranteHorizontal = null;
    private Direction directionCouranteVertical = null;
    private static ControleColonneDeplacement colonne;
    
    private int type;

    public static ControleColonneDeplacement getInstance() {
        if (colonne == null) {
            colonne = new ControleColonneDeplacement();
        }
        return colonne;
    }
    
    public void setType(int type){
        this.type = type;
    }
    
    public void reset(){
        directionCouranteHorizontal = null;
        directionCouranteVertical = null;
    }
    
    public void changeDirectionCourante(){
        directionCouranteVertical = (directionCouranteVertical == Direction.bas) ? Direction.haut : Direction.bas;
        directionCouranteHorizontal = (directionCouranteHorizontal == Direction.droite) ? Direction.gauche : Direction.droite;
    }
    
    @Override
    public boolean realiserDeplacement() {
        boolean ret = false;
        Direction directionCourante; 
        Colonne colonneActuel;
        EntiteDynamique e;
        
        int nbColonne = lstEntitesDynamiques.size();
        for (int i = 0; i < nbColonne; i++) {
       
                    
            if (directionCouranteVertical == Direction.haut || directionCouranteHorizontal == Direction.gauche) {
                e = lstEntitesDynamiques.get(i);
            } else {
                 e = lstEntitesDynamiques.get(nbColonne - 1 - i);
            }

            colonneActuel = (Colonne) e;
            if(colonneActuel.getDirection() == 0){
                directionCourante = directionCouranteVertical;
            }
            else{
                directionCourante = directionCouranteHorizontal;
            }

            if (colonneActuel.getType() == this.type) {
                if(directionCourante != null){
                    Entite eDirection = e.regarderDansLaDirection(directionCourante);
                    if(eDirection instanceof EntiteDynamique){
                        EntiteDynamique eDynamique = (EntiteDynamique) eDirection;
                        if(eDynamique != null && eDynamique.peutEtreEcrase()){
                            eDynamique.avancerDirectionChoisie(directionCourante);
                        }
                    }
                    
                    e.avancerDirectionChoisie(directionCourante);
                    ret = true;
                }
            }
        }
        return ret; 
    } 
}

